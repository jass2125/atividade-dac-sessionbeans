/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac.dao;

import edu.ifpb.dac.beans.util.ConnectionFactory;
import edu.ifpb.dac.entidades.Produto;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Anderson Souza
 */
public class ProdutoDaoImpl implements ProdutoDao {

    private ConnectionFactory conFactory;

    public ProdutoDaoImpl() {
        conFactory = new ConnectionFactory();
    }

    @Override
    public void add(Produto p) throws SQLException, ClassNotFoundException {
        String sql = "insert into produto(descricao, preco) values(?, ?);";
        Connection con = conFactory.getConnection();
        PreparedStatement ps = con.prepareCall(sql);
        ps.setString(1, p.getDescricao());
        ps.setBigDecimal(2, p.getPreco());
        ps.execute();
    }

    @Override
    public List<Produto> list() throws SQLException, ClassNotFoundException {
        String sql = "select * from produto;";
        Connection con = conFactory.getConnection();
        PreparedStatement ps = con.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        List<Produto> listProdutos = new ArrayList<>();
        while (rs.next()) {
            int id = rs.getInt("id");
            String descricao = rs.getString("descricao");
            BigDecimal preco = rs.getBigDecimal("preco");
            Produto produto = new Produto(id, descricao, preco);
            listProdutos.add(produto);
            produto = null;
        }
        return listProdutos;
    }

    @Override
    public Produto find(int id) throws SQLException, ClassNotFoundException {
        String sql = "select * from produto where id = ?;";
        Connection con = conFactory.getConnection();
        PreparedStatement ps = con.prepareCall(sql);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Produto produto = null;

        while (rs.next()) {
            String descricao = rs.getString("descricao");
            BigDecimal preco = rs.getBigDecimal("preco");
            produto = new Produto(id, descricao, preco);
        }
        return produto;
    }
}

//create table produto(
//    id int auto_increment,
//    descricao varchar(50),
//    preco numeric(10,2),
//    primary key(id)
//);
