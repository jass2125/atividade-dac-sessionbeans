/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac.dao;

import edu.ifpb.dac.entidades.Produto;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Anderson Souza
 */
public interface ProdutoDao {

    public void add(Produto p) throws SQLException, ClassNotFoundException;

    public List<Produto> list() throws SQLException, ClassNotFoundException;

    public Produto find(int id) throws SQLException, ClassNotFoundException;

}
