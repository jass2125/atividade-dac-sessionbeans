/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac.beans.interfaces;

import edu.ifpb.dac.entidades.Produto;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Anderson Souza
 */
public interface PedidoService {

    public void add(Produto p);

    public List<Produto> list();

    public BigDecimal calculaTotal();
}
