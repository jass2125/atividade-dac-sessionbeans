/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac.beans;

import edu.ifpb.dac.beans.interfaces.PedidoService;
import edu.ifpb.dac.dao.ProdutoDao;
import edu.ifpb.dac.dao.ProdutoDaoImpl;
import edu.ifpb.dac.entidades.Pedido;
import edu.ifpb.dac.entidades.Produto;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateful;

/**
 *
 * @author Anderson Souza
 */
@Stateful
public class PedidoServiceImpl implements PedidoService {

    private final Pedido pedido;
    private final ProdutoDao dao;

    public PedidoServiceImpl() {
        dao = new ProdutoDaoImpl();
        pedido = new Pedido();
    }

    @Override
    public void add(Produto p) {
        pedido.add(p);
    }

    public void remove(Produto p) {
        pedido.remove(p);
    }

    @Override
    public List<Produto> list() {
        return pedido.getProdutos();
    }

    @Override
    public BigDecimal calculaTotal() {
        BigDecimal total = new BigDecimal(0);
        for (Produto it : list()) {
            total = total.add(it.getPreco());
        }
        return total;
    }

}
