/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac.beans;

import edu.ifpb.dac.beans.interfaces.ProdutoService;
import edu.ifpb.dac.dao.ProdutoDao;
import edu.ifpb.dac.dao.ProdutoDaoImpl;
import edu.ifpb.dac.entidades.Produto;
import java.sql.SQLException;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author Anderson Souza
 */
@Stateless
public class ProdutoServiceImpl implements ProdutoService {

    private ProdutoDao dao;

    public ProdutoServiceImpl() {
        dao = new ProdutoDaoImpl();
    }

    @Override
    public void add(Produto p) {
        try {
            dao.add(p);
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<Produto> list() {
        try {
            return dao.list();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Produto find(int id) {
        try {
            return dao.find(id);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

}
