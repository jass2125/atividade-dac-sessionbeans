/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac.controlador;

import edu.ifpb.dac.beans.interfaces.PedidoService;
import edu.ifpb.dac.beans.interfaces.ProdutoService;
import edu.ifpb.dac.entidades.Produto;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Anderson Souza
 */
@WebServlet(urlPatterns = {"/addiCarrinho"})
public class Carrinho extends HttpServlet {

    @EJB
    private PedidoService pedidoService;
    @EJB
    private ProdutoService produtoService;

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        Produto produto = produtoService.find(id);
        pedidoService.add(produto);
        req.getSession().setAttribute("produtos", produtoService.list());
        req.getSession().setAttribute("total", pedidoService.calculaTotal());
        resp.sendRedirect("index.jsp");

    }

}
