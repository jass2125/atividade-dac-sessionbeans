/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac.controlador;

import edu.ifpb.dac.beans.interfaces.ProdutoService;
import edu.ifpb.dac.entidades.Produto;
import java.io.IOException;
import java.math.BigDecimal;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Anderson Souza
 */
@WebServlet(urlPatterns = {"/addProduto"})
public class ProdutoControlador extends HttpServlet {
    @EJB
    private ProdutoService service;
    
    private Produto produto;

    
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        produto = new Produto(req.getParameter("produto"), BigDecimal.valueOf(Double.parseDouble(req.getParameter("preco"))));
        service.add(produto);
        req.getSession().setAttribute("produtos", service.list());
        resp.sendRedirect("index.jsp");
    }

}
