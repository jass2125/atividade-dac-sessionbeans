<%-- 
    Document   : newjsp
    Created on : Apr 25, 2016, 8:47:20 PM
    Author     : jairanderson
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Sesion Beans</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/modern-business.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">    
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>
        <!-- Modal -->
        <div id="register" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content - Modal do Cadastro -->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Cadastro de Produto</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form" action="addProduto" method="post">
                            <div class="form-group">
                                <label for="username">Descrição</label>
                                <input type="text" name="produto" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="nome">Preço</label>
                                <input type="text" name="preco" class="form-control" id="nome" required="true">
                            </div>
                            <button type="submit" class="btn btn-success">Cadastrar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Produtos</h1>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <a href="" data-toggle="modal" data-target="#register"><span class="glyphicon glyphicon-plus"></span></a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="table-responsive">
                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>Descrição</th>
                            <th>Preço</th>
                            <th>Adicionar ao meu carrinho</th>
                        </tr>
                    </thead>
                    <tbody id="table">
                        <c:forEach items="${sessionScope.produtos}" var="produto">
                            <tr>
                                <td>${produto.descricao}</td>
                                <td>${produto.preco}</td>
                                <td>
                                    <form action="addiCarrinho?id=${produto.id}" method="post">
                                        <button type="submit" class="glyphicon glyphicon-pencil btn btn-default"></button>
                                    </form>
                                </td>
                            </c:forEach>
                    </tbody>
                </table>
            </div> 
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Carrinho de Compras</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="table-responsive">
                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>Descrição</th>
                            <th>Preço</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody id="table">
                        <c:forEach items="${sessionScope.produtos}" var="produto">
                            <tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                                <td>${produto.descricao}</td>
                                <td>${produto.preco}</td>

                            </c:forEach>
                            <td>${sessionScope.total}</td>
                        </tr>
                    </tbody>
                </table>
                <button class=" btn btn-success">Ver Total</button> 
            </div> 
        </div>


    </body>
</html>
